import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
/**
 * Generated class for the TouristDetails page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tourist-details',
  templateUrl: 'tourist-details.html',
})
export class TouristDetails {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TouristDetails');
  }

}
