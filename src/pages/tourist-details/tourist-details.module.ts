import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TouristDetails } from './tourist-details';

@NgModule({
  declarations: [
    TouristDetails,
  ],
  imports: [
    IonicPageModule.forChild(TouristDetails),
  ],
})
export class TouristDetailsModule {}
