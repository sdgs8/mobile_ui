import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { TouristDetails } from '../../pages/tourist-details/tourist-details';
/**
 * Generated class for the Tourist page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-tourist',
  templateUrl: 'tourist.html',
})
export class Tourist {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Tourist');
  }
getTouristDetails(){
  console.log("done");
this.navCtrl.push(TouristDetails)
}
}
