import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Tourist } from './tourist';

@NgModule({
  declarations: [
    Tourist,
  ],
  imports: [
    IonicPageModule.forChild(Tourist),
  ],
})
export class TouristModule {}
