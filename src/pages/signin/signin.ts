import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
/**
 * Generated class for the Signin page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class Signin {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signin');
  }

}
