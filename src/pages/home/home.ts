import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {Observable} from 'rxjs/Observable';
import {} from '@types/googlemaps';
import { Reservations } from '../../pages/reservations/reservations';
import { Signin } from '../../pages/signin/signin';
declare var google : any;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
public map;
public marker;
public myMarker;
public Search;
public SearchStatus = false;

  constructor(public navCtrl: NavController,public geolocation: Geolocation, public navParams: NavParams, public loadingCtrl: LoadingController ) {

  }
ngOnInit(){
this.map=this.createMap();
this.getCurrentLocation().subscribe(location =>{
  this.centerLocation(location);
  this.CurrentLocationMarker(location);
});
}
createMap(location= new google.maps.LatLng(3.568248, 1.933594)){
   //Create Map
let mapOptions = {
  center: location,
  zoom: 15,
  mapTypeId: google.maps.MapTypeId.TERRAIN,
  disableDefaultUI:true

}
let mapEL = document.getElementById('map');
 return new google.maps.Map(mapEL, mapOptions)
  }
getCurrentLocation(){
    //get Current Location
    console.log("function")
      let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  loading.present();
     let locationObs=Observable.create(observer =>{
      let options = {timeout: 10000, enableHighAccuracy: true};
     this.geolocation.getCurrentPosition(options)
     .then(resp => {
          let lat = resp.coords.latitude;
          let lng = resp.coords.longitude;

          let location = new google.maps.LatLng(lat, lng);

          console.log('Geolocation: ' + location);

          observer.next(location);


        },
        (err) => {
          console.log('Geolocation err: ' + err);
        })
    })
    loading.dismiss();
    return locationObs;
  }
centerLocation(location) {
     let loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
    //move map to current location
    if (location) {
     // this.currentlocation=location;
      this.map.panTo(location);

    }
    else {

      this.getCurrentLocation().subscribe(currentLocation => {
        //this.currentlocation=currentLocation;
        //Moves map to users current location
        this.map.panTo(currentLocation);
      });
    }
    loading.dismiss();
  }
  CurrentLocationMarker(location){
    var that=this;
    this.myMarker = new google.maps.Marker({
          position: location,
          map: that.map,
          draggable: true,
    animation: google.maps.Animation.DROP
        });
  }
  SearchAction(){
    this.SearchStatus=!this.SearchStatus;
  }
  getReservations(){
    this.navCtrl.push(Reservations);
  }
  getSignin(){
    this.navCtrl.push(Signin);
  }
}
