import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Tourist } from '../../pages/tourist/tourist';
/**
 * Generated class for the Reservations page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-reservations',
  templateUrl: 'reservations.html',
})
export class Reservations {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Reservations');
  }
getTourist(){
console.log("test");
this.navCtrl.push(Tourist);
}
}
