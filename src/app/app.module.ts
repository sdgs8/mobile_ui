import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Reservations } from '../pages/reservations/reservations';
import { Tourist } from '../pages/tourist/tourist';
import { Report } from '../pages/report/report';
import { Signin } from '../pages/signin/signin';
import { TouristDetails } from '../pages/tourist-details/tourist-details';
import { ListPage } from '../pages/list/list';

import {} from '@types/googlemaps';
import {Geolocation} from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    Reservations,
    Tourist,
    Report,
    Signin,
    TouristDetails
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    Reservations,
    Tourist,
    Report,
    Signin,
    TouristDetails
  ],
  providers: [
    StatusBar,
    SplashScreen,
  Geolocation,
    NativeGeocoder,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
